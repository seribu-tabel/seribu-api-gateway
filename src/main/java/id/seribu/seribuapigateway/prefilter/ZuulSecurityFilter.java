package id.seribu.seribuapigateway.prefilter;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.client.RestTemplate;

import id.seribu.seribuapigateway.constant.DefaultConstant;

public class ZuulSecurityFilter extends ZuulFilter {

    private static final Logger log = LoggerFactory.getLogger(ZuulSecurityFilter.class);

    @LoadBalanced
    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.httpResponseHeaders}")
    private String httpResponseHeader;
    @Value("${client.httpResponseHeadersValue}")
    private String httpResponseHeaderValue;

    @Override
    public Object run() throws ZuulException {
        log.debug("Execute AuthenticationFilter.run(); BEGIN");
        final RequestContext ctx = RequestContext.getCurrentContext();
        ctx.setDebugRouting(true);
        ctx.setDebugRequest(true);
        log.debug("Execute AuthenticationFilter.run(); END");
        final String[] headersResponse = httpResponseHeader.split(DefaultConstant.COMMA_DELIMITER_CHAR);
        final String[] headersResponseValue = httpResponseHeaderValue.split(DefaultConstant.COMMA_DELIMITER_CHAR);

        for (int i = 0; i < headersResponse.length; i++) {
            ctx.addZuulResponseHeader(headersResponse[i], headersResponseValue[i]);
        }
        final HttpServletRequest request = ctx.getRequest();
        log.info("{} request to {}", request.getMethod(), request.getRequestURL());
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public String filterType() {
        return "pre";
    }


}