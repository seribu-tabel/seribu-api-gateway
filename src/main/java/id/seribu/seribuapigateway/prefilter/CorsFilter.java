package id.seribu.seribuapigateway.prefilter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
@Configuration
public class CorsFilter implements WebMvcConfigurer {
    
    @Value("${client.allowedHeaders}")
    private String[] allowedHeaders;
    @Value("${client.allowedMethods}")
    private String[] allowedMethod;

    @Override
    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**")
                    .allowedOrigins("*")
                    .allowedHeaders("origin"
                        , "X-Requested-With"
                        , "Content-Disposition"
                        , "content-type"
                        , "accept"
                        , "Authorization"
                        , "Access-Control-Allow-Origin"
                        , "Access-Control-Allow-Headers"
                        , "Access-Control-Expose-Headers")
                    .allowedMethods("GET","POST","DELETE","PUT","HEAD","OPTIONS");
    }
}