package id.seribu.seribuapigateway.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class StringUtil{

    public static List<String> parseStringDelimiter(final String strValue, final String delimiter){
        final List<String> strList = new ArrayList<>();
        final StringTokenizer st = new StringTokenizer(strValue, delimiter);
        while(st.hasMoreElements()){
            final String value = st.nextToken();
            strList.add(value.trim());
        }
        return strList;
    }
}