package id.seribu.seribuapigateway.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public final class AntPathRequestMatcher{

    private static final Logger log = LoggerFactory.getLogger(AntPathRequestMatcher.class);
    private static final String MATCH_ALL = "/**";

    private final Matcher matcher;
    private final String pattern;
    private final HttpMethod httpMethod;

    public AntPathRequestMatcher(String pattern){
        this(pattern, null);
    }

    public AntPathRequestMatcher(String pattern, String httpMethod){
        Assert.hasText(pattern, "Pattern cannot be null ot empty");

        if(pattern.equals(MATCH_ALL) || "**".equals(pattern)){
            pattern = MATCH_ALL;
            matcher = null;
        }else{
            pattern = pattern.toLowerCase();
            if(pattern.endsWith(MATCH_ALL) && pattern.indexOf('?') == -1
                    && pattern.indexOf('*') == pattern.length() - 2){
                matcher = new SubPathMatcher(pattern.substring(0, pattern.length() - 3));
            }else{
                matcher = new SpringAntMatcher(pattern);
            }
        }
        this.pattern = pattern;
        this.httpMethod = StringUtils.hasText(httpMethod) ? HttpMethod.valueOf(httpMethod) : null;
    }

    public boolean matches(HttpServletRequest request){
        if(httpMethod != null && httpMethod != HttpMethod.valueOf(request.getMethod())){
            if(log.isDebugEnabled()){
                log.debug("Request '{} {}' doesn't match '{} {}", request.getMethod(), getRequestPath(request), httpMethod, pattern);
            }
            return false;
        }

        if(pattern.equals(MATCH_ALL)){
            if(log.isDebugEnabled()){
                log.debug("Request '{}' matched by universal pattern '/**'", getRequestPath(request));
            }
            return true;
        }

        String url = getRequestPath(request);

        if(log.isDebugEnabled()){
            log.debug("checking match of request: {}; against '{}'", url, pattern);
        }
        return matcher.matches(url);
    }

    public String getRequestPath(HttpServletRequest request){
        String url = request.getServletPath();
        if(request.getPathInfo() != null){
            url += request.getPathInfo();
        }
        url = url.toLowerCase();
        return url;
    }

    public String getPattern(){
        return pattern;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof AntPathRequestMatcher)){
            return false;
        }
        AntPathRequestMatcher other = (AntPathRequestMatcher) obj;
        return this.pattern.equals(other.pattern) && this.httpMethod == other.httpMethod;
    }

    @Override
    public int hashCode(){
        int code = 31 ^ pattern.hashCode();
        if(httpMethod != null){
            code ^= httpMethod.hashCode();
        }
        return code;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Ant [pattern='").append(pattern).append("'");

        if(httpMethod != null){
            sb.append(", ").append(httpMethod);
        }

        sb.append("]");
        return sb.toString();
    }

    private static interface Matcher{
        boolean matches(String path);
    }

    private static class SpringAntMatcher implements Matcher{

        private static final AntPathMatcher antMatcher = new AntPathMatcher();
        private final String pattern;

        private SpringAntMatcher(String pattern){
            this.pattern = pattern;
        }
        @Override
        public boolean matches(String path) {
            return antMatcher.match(pattern, path);
        }
    }

    private static class SubPathMatcher implements Matcher{
        private final String subpath;
        private final int length;

        private SubPathMatcher(String subpath){
            assert !subpath.contains("*");
            this.subpath = subpath;
            this.length = subpath.length();
        }

        @Override
        public boolean matches(String path) {
            return path.startsWith(subpath) && (path.length() == length || path.charAt(length) == '/');
        }
    }
}