package id.seribu.seribuapigateway;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import id.seribu.seribuapigateway.constant.DefaultConstant;
import id.seribu.seribuapigateway.prefilter.CorsFilter;
import id.seribu.seribuapigateway.prefilter.ZuulSecurityFilter;
import id.seribu.seribuapigateway.util.AntPathRequestMatcher;
import id.seribu.seribuapigateway.util.StringUtil;


@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@RefreshScope
@EnableCircuitBreaker
public class SeribuApiGatewayApplication extends SpringBootServletInitializer{

	private static final Logger log = LoggerFactory.getLogger(SeribuApiGatewayApplication.class);

	@Value("${client.exceptionUrlPatterns}")
	private String exceptionUrlPatterns;

	public static void main(String[] args) {
		SpringApplication.run(SeribuApiGatewayApplication.class, args);
	}

	@LoadBalanced
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}

	@Bean
	public ZuulSecurityFilter authenticationFilter(){
		return new ZuulSecurityFilter();
	}

	@Bean
	public ServletContextInitializer servletContextInitializer(@Value("${secure.cookie}") final boolean secure){
		return servletContext -> servletContext.getSessionCookieConfig().setSecure(secure);
	}

	@Bean
	public CorsFilter corsConfiguration(){
		return new CorsFilter();
	}

	public AntPathRequestMatcher[] permitAllRequestMatchers(){
		AntPathRequestMatcher[] permitAllRequestMatchers = null;

		final List<String> urlPattern = StringUtil.parseStringDelimiter(exceptionUrlPatterns, DefaultConstant.COMMA_DELIMITER_CHAR);
		if(urlPattern != null && !urlPattern.isEmpty()){
			permitAllRequestMatchers = new AntPathRequestMatcher[urlPattern.size()];
			for(int i = 0; i < urlPattern.size(); i++){
				permitAllRequestMatchers[i] = new AntPathRequestMatcher(urlPattern.get(i));
				log.info("permitRequestMatchers {}: {}", i +1, permitAllRequestMatchers[i]);
			}
		}
		return permitAllRequestMatchers;
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		return application.sources(SeribuApiGatewayApplication.class);
	}
}
